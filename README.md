Role Matlab
=========

Ansible role to install silently Comsol 6.8 with licenses.

Compatibility
------------

This role can be used only on Ubuntu 18. Other OS will come later

Variables Used
------------

You can find in defaults/main.yml all variables used in tasks

| Variable                 | Default Value                                                | Type   | Description                                                  |
| ------------------------ | :----------------------------------------------------------- | :----- | ------------------------------------------------------------ |
| comsol_repo_base_url     | https://s3.epfl.ch/10282-70726cc2c34fcfe7d4325ef7ea151411    | String | S3 Bucket                                                    |
| comsol_install_version   | comsol                                                       | String | Default version of matlab to install                         |
| comsol_versions          | -                                                            | Dict   | Dictionnary for getting the url and password for the zip to download |
| url                      | comsol.zip                                                   | String | Url on the S3 to get the wanted zip                          |
| password                 | -                                                            | String | Encrypted password to unarchive the zip file                 |
| installed_path           | /usr/local/comsol                                            | String | Matlab installation folder                                   |
| installer_input_comsol   | /tmp/setup_comsol.ini | String                               | Path for the Input file for installation                     |
| comsol_install_destination | /tmp                                                       | String | Path for the propertiesFile for installation                 |
| comsol_tmp_setup         | /tmp/comsol/setup                                            | String | Path for the license.dat                                     |
| archive_path             | /tmp/comsol                                                  | String | Temporary folder where matlab installer is stored            |

## Author Information

Written by [Pullyvan Krishnamoorthy ](mailto:pullyvan.krishnamoorthy@epfl.ch) for EPFL - STI school of engineering